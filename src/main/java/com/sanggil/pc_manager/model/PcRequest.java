package com.sanggil.pc_manager.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.GeneratedValue;

@Getter
@Setter
public class PcRequest {
    private String name;
    private String pc_name;
}
