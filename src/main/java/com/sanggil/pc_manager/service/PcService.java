package com.sanggil.pc_manager.service;

import com.sanggil.pc_manager.entity.Pc;
import com.sanggil.pc_manager.repository.PcRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PcService {
    private final PcRepository pcRepository;

    public void setPc (String name, String pc_name) {
        Pc addData = new Pc();
        addData.setName(name);
        addData.setPc_name(pc_name);

        pcRepository.save(addData);
    }
}
