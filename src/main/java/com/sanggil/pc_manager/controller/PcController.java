package com.sanggil.pc_manager.controller;

import com.sanggil.pc_manager.model.PcRequest;
import com.sanggil.pc_manager.service.PcService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/pc")
public class PcController {
    private final PcService pcService;

    @PostMapping("/data")
    public String setPc(@RequestBody PcRequest request) {
        pcService.setPc(request.getName(), request.getPc_name());
        return "GOOD";
    }
}
