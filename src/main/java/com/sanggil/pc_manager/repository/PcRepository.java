package com.sanggil.pc_manager.repository;

import com.sanggil.pc_manager.entity.Pc;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PcRepository extends JpaRepository<Pc, Long> {
}
